variable "node_group_iam_role_arn" {
  description = "The IAM role ARN for worker node group"
  type        = string
  default     = "arn://1234example"
}

variable "user_iam_role_arn" { 
  description = "The IAM role ARN for the users of the cluster"
  type = string
  default = "arn://1234example"
}

variable "eks_cluster_endpoint" {
  description = "The EKS cluster endpoint"
  type = string
  default = null
}

variable "eks_cluster_ca" {
  description = "The EKS cluster certificate authority data"
  type = string
  default = null
}

variable "eks_cluster_id" {
  description = "ID of EKS cluster"
  type = string
  default = null
}

variable "config_path" {
  description = "Path to EKS config file"
  type = string
  default = null
}
